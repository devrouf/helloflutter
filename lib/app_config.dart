import 'package:flutter/material.dart';

class AppConfig extends InheritedWidget {
  AppConfig(
      this.tag,
      this.host,
      Widget child,
      ) : super(child: child);

  final String tag;
  final String host;

  bool get isProd => tag.toLowerCase() == "prod";

  static AppConfig? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<AppConfig>();
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}