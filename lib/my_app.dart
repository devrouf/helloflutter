import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:helloworld/app_config.dart';
import 'package:helloworld/contoh_drawer.dart';
import 'package:helloworld/contoh_gridview.dart';
import 'package:helloworld/contoh_listview.dart';
import 'package:helloworld/contoh_login.dart';
import 'package:helloworld/contoh_persistence.dart';
import 'package:helloworld/contoh_popupmenubutton.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
    // return GetMaterialApp(
      home: ContohPertemuan(),
    );
  }
}

class ContohPertemuan extends StatelessWidget {
  const ContohPertemuan({Key? key}) : super(key: key);

  Widget listTile(BuildContext context, String title, Widget page) {
    return ListTile(
      title: Text(title),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (buildContext) {
            return page;
          }),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("SFA 2021")),
      body: ListView(
        children: [
          listTile(
            context,
            "Contoh PopupMenuButton",
            ContohPopupMenuButton(),
          ),
          listTile(
            context,
            "Contoh ListView",
            Scaffold(
              appBar: AppBar(title: Text("Contoh ListView")),
              body: ContohListView(),
            ),
          ),
          listTile(
            context,
            "Contoh GridView",
            Scaffold(
              appBar: AppBar(title: Text("Contoh GridView")),
              body: ContohGridView(),
            ),
          ),
          listTile(
            context,
            "Contoh Drawer",
            ContohDrawer(),
          ),
          listTile(
            context,
            "Demo Pertemuan 05 - Drawer",
            DemoPertemuan5(),
          ),
          listTile(
            context,
            "Contoh Perbandingan State",
            Scaffold(
              appBar: AppBar(title: Text("Contoh Perbandingan State")),
              body: Column(
                children: [
                  Expanded(child: ContohStateless()),
                  Expanded(child: ContohStateful()),
                ],
              ),
            ),
          ),
          listTile(
            context,
            "Contoh tanpa State Management",
            Coba(),
          ),
          listTile(
            context,
            "Contoh State Management",
            ContohSM(),
          ),
          listTile(
            context,
            "Contoh Persistence",
            ContohPersistence(),
          ),
          listTile(
            context,
            "Contoh Login",
            ContohLogin(),
          ),
        ],
      ),
    );
  }
}


class ContohStateless extends StatelessWidget {
  ContohStateless({Key? key}) : super(key: key);
  final int nilai = 0;
  final Data data = Data();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("SFA - Contoh Stateless")),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),
        onPressed: () {
          // nilai++;
          data.nilai++;
          // setState(() {});

          if (data.nilai >= 3) {
            showDialog(
              context: context,
              builder: (bc) => AlertDialog(
                title: Text("Alert ${data.nilai}"),
              ),
            );
            // ).then((value) => nilai = 0);
            // nilai = 0;
          } else {
            data.nilai++;
          }
        },
      ),
      body: Center(child: Column(
        children: [
          Text("nilai $nilai"),
          Text("nilai ${data.nilai}"),
        ],
      )),
    );
  }
}

class ContohStateful extends StatefulWidget {
  const ContohStateful({Key? key}) : super(key: key);

  @override
  _ContohStatefulState createState() => _ContohStatefulState();
}

class _ContohStatefulState extends State<ContohStateful> {
  int nilai = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("SFA - Contoh Stateful")),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),
        onPressed: () {
          if (nilai >= 3) {
            showDialog(
              context: context,
              builder: (bc) => AlertDialog(
                title: Text("Alert $nilai"),
              ),
            ).then((value) => nilai = 0);
            // );
            // nilai = 0;
          } else {
            nilai++;
          }
          setState(() {});
        },
      ),
      body: Center(child: Text("nilai $nilai")),
    );
  }
}

class ContohSMController extends GetxController {
  RxList<Widget> tiles = [
    BoxWarna(key: UniqueKey()),
    BoxWarna(key: UniqueKey()),
    BoxWarna(key: UniqueKey()),
    // BoxWarna(),
    // BoxWarna(),
    // BoxWarna(),
    // BoxWarna(),
  ].obs;

  void updateTiles() {
    tiles.add(tiles.removeAt(0));
    // refresh();
  }
}

// class ContohSM extends GetView<ContohSMController> {
class ContohSM extends StatelessWidget {
  final controller = Get.put(ContohSMController());

  @override
  Widget build(BuildContext context) {
    print("rebuild");
    return Scaffold(
      appBar: AppBar(title: Text("SFA 2021 - SM GetX")),
      body: Obx(() => Column(
        children: [
          Text(AppConfig.of(context)?.host ?? ""),
          Expanded(child: Row(children: controller.tiles,)),
          Expanded(child: ListView(children: controller.tiles,)),
          controller.tiles[0],
        ],
      )),
      // body: SafeArea(child: Obx(() => Row(children: controller.tiles,))),
      // body: SafeArea(child: Obx(() => ListView(children: controller.tiles,))),
      // body: SafeArea(child: Obx(() {
      //   print("obx rebuild");
      //   return ListView(children: controller.tiles,);
      // })),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          controller.updateTiles();
        },
        child: Icon(Icons.swap_horiz),
      ),
    );
  }
}

class UniqueColorGenerator {
  static Random random = new Random();
  static Color getColor() {
    return Color.fromARGB(
        255, random.nextInt(255), random.nextInt(255), random.nextInt(255));
  }
}

class Coba extends StatefulWidget {
  const Coba({Key? key}) : super(key: key);

  @override
  _CobaState createState() => _CobaState();
}

class _CobaState extends State<Coba> {
  List<Widget> tiles = [
    // BoxWarna(key: UniqueKey()),
    // BoxWarna(key: UniqueKey()),
    BoxWarna(),
    BoxWarna(),
  ];

  @override
  Widget build(BuildContext context) {
    print("rebuild");
    return Scaffold(
      appBar: AppBar(title: Text("SFA 2021 - State")),
      body: SafeArea(
        child: Column(children: [
          Expanded(child: Row(children: tiles)),
          Expanded(child: ListView(children: tiles)),
          tiles[0],
        ]),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            tiles.insert(1, tiles.removeAt(0));
          });
        },
        child: Icon(Icons.swap_horiz),
      ),
    );
  }
}

class BoxWarna extends StatefulWidget {
  // const BoxWarna({Key? key}) : super(key: key);
  BoxWarna({Key? key}) : super(key: key);
  final Color _color = UniqueColorGenerator.getColor();

  @override
  _BoxWarnaState createState() => _BoxWarnaState();
}

class _BoxWarnaState extends State<BoxWarna> {
  // Color _color = UniqueColorGenerator.getColor();
  Color? _color;

  @override
  void initState() {
    super.initState();
    _color = UniqueColorGenerator.getColor();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: _color,
      color: widget._color,
      width: 128,
      height: 128,
    );
  }
}


class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  @override
  void initState() {
    WidgetsBinding.instance?.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('state = $state');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Tutorial Lifecycle'),
      ),
      body: Center(),
    );
  }
}

class DemoPertemuan5 extends StatefulWidget {
  const DemoPertemuan5({Key? key}) : super(key: key);

  @override
  _DemoPertemuan5State createState() => _DemoPertemuan5State();
}

class _DemoPertemuan5State extends State<DemoPertemuan5> {
  List<Data> listData = [];

  int indexMenu = 0;

  Widget _contohDrawer() {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            child: Container(
              color: Colors.green,
            ),
          ),
          ListTile(
            leading: Icon(Icons.people),
            title: Text("Contoh listView"),
            onTap: () {
              indexMenu = 1;
              Navigator.pop(context);
              setState(() {});
            },
          ),
          ListTile(
            leading: Icon(Icons.inbox),
            title: Text("Contoh GridView"),
            onTap: () {
              indexMenu = 2;
              Navigator.pop(context);
              setState(() {});
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          if (indexMenu == 0)
            Expanded(
              child: ListView.builder(
                itemCount: listData.length,
                itemBuilder: (bc, idx) {
                  final data = listData[idx];
                  return ListTile(
                    title: Text(data.produk ?? ""),
                    subtitle: Text("Qty: ${data.qty}"),
                    // trailing: Icon(Icons.more_horiz),
                    trailing: PopupMenuButton<int>(
                      itemBuilder: (buildContext) {
                        return [
                          PopupMenuItem(child: Text("Hapus"), value: 0),
                          PopupMenuItem(child: Text("Ubah"), value: 1),
                        ];
                      },
                      onSelected: (value) {
                        if (value == 0) {
                          print("hapus $value");
                          listData.removeAt(idx);
                        } else if (value == 1) {
                          print("ubah $value");
                          data.qty = Random().nextInt(20) + 20;
                        }
                        setState(() {});
                      },
                    ),
                  );
                },
              ),
            ),
          if (indexMenu == 1) Expanded(child: ContohListView()),
          if (indexMenu == 2) Expanded(child: ContohGridView()),
        ],
      ),
      drawer: _contohDrawer(),
      endDrawer: _contohDrawer(),
      appBar: AppBar(
        title: Text("Pertemuan 5"),
        actions: [
          IconButton(
            onPressed: () {
              int qty = Random().nextInt(20);
              listData.add(Data(
                produk: "Semen $qty",
                qty: qty,
              ));
              setState(() {});
            },
            icon: Icon(Icons.add),
          ),
        ],
      ),
    );
  }
}

class Data {
  String? produk;
  int? qty;
  int nilai = 0;

  Data({this.produk, this.qty});
}
