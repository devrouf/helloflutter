/*
 * Create by roufroufrouf on 2021
 * Email: roufroufrouf@gmail.com
 * Last modified 9/16/21, 7:44 PM
 */

import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ContohPersistence extends StatefulWidget {
  const ContohPersistence({Key? key}) : super(key: key);

  @override
  _ContohPersistenceState createState() => _ContohPersistenceState();
}

class _ContohPersistenceState extends State<ContohPersistence> {
  SharedPreferences? pref;
  String? data1;
  String? data2;
  String? data3;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Data Persistence")),
      body: Column(
        children: [
          Text("GetStorage"),
          Text("Data1 = $data1"),
          Row(
            children: [
              ElevatedButton(
                onPressed: () async {
                  await GetStorage.init();
                  print("init get storage");
                },
                child: Text("Init"),
              ),
              ElevatedButton(
                onPressed: () {
                  GetStorage pref = GetStorage();
                  pref.write("token", "Bearer ${DateTime.now().millisecondsSinceEpoch}");
                },
                child: Text("Set"),
              ),
              ElevatedButton(
                onPressed: () {
                  GetStorage pref = GetStorage();
                  print("token ${pref.read("token")}");
                  setState(() {
                    data1 = pref.read("token");
                  });
                },
                child: Text("Get"),
              ),
              ElevatedButton(
                onPressed: () {
                  GetStorage pref = GetStorage();
                  pref.erase();
                },
                child: Text("Clear"),
              ),
            ],
          ),
          SizedBox(height: 48),
          Text("Shared Preferences 1"),
          Text("Menggunakan init setiap pemanggilan"),
          Text("Data2 = $data2"),
          Row(
            children: [
              ElevatedButton(
                onPressed: () async {
                  print("tidak ada init");
                },
                child: Text("Init"),
              ),
              ElevatedButton(
                onPressed: () async {
                  SharedPreferences pref = await SharedPreferences.getInstance();
                  pref.setString("token", "Bearer ${DateTime.now().millisecondsSinceEpoch}");
                },
                child: Text("Set"),
              ),
              ElevatedButton(
                onPressed: () async {
                  SharedPreferences pref = await SharedPreferences.getInstance();
                  print("token ${pref.getString("token")}");
                  setState(() {
                    data2 = pref.getString("token");
                  });
                },
                child: Text("Get"),
              ),
              ElevatedButton(
                onPressed: () async {
                  SharedPreferences pref = await SharedPreferences.getInstance();
                  pref.clear();
                },
                child: Text("Clear"),
              ),
            ],
          ),
          SizedBox(height: 48),
          Text("Shared Preferences 3"),
          Text("Tanpa init setiap pemanggilan,"),
          Text("namun wajib init di awal penggunaan"),
          Text("Data3 = $data3"),
          Row(
            children: [
              ElevatedButton(
                onPressed: () async {
                  pref = await SharedPreferences.getInstance();
                },
                child: Text("Init"),
              ),
              ElevatedButton(
                onPressed: () {
                  pref?.setString("token", "Bearer ${DateTime.now().millisecondsSinceEpoch}");
                },
                child: Text("Set"),
              ),
              ElevatedButton(
                onPressed: () {
                  print("token ${pref?.getString("token")}");
                  setState(() {
                    data3 = pref?.getString("token");
                  });
                },
                child: Text("Get"),
              ),
              ElevatedButton(
                onPressed: () async {
                  SharedPreferences pref = await SharedPreferences.getInstance();
                  pref.clear();
                },
                child: Text("Clear"),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

