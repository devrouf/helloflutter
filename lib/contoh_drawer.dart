/*
 * Create by roufroufrouf on 2021
 * Email: roufroufrouf@gmail.com
 * Last modified 9/2/21, 9:15 PM
 */

import 'package:flutter/material.dart';

class ContohDrawer extends StatefulWidget {
  const ContohDrawer({Key? key}) : super(key: key);

  @override
  _ContohDrawerState createState() => _ContohDrawerState();
}

class _ContohDrawerState extends State<ContohDrawer> {
  Widget _contohDrawer() {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            child: Container(
              color: Colors.green,
            ),
          ),
          ListTile(
            leading: Icon(Icons.people),
            title: Text("Daftar Pelanggan"),
          ),
          ListTile(
            leading: Icon(Icons.inbox),
            title: Text("Inbox"),
          ),
          ...List.generate(
            15,
            (index) => ListTile(
              leading: Icon(Icons.inbox),
              title: Text("Inbox"),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _contohDrawerFix() {
    return Drawer(
      child: Column(
        children: [
          DrawerHeader(
            child: Container(
              color: Colors.green,
            ),
          ),
          Expanded(
            child: ListView(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              children: [
                ListTile(
                  leading: Icon(Icons.people),
                  title: Text("Daftar Pelanggan"),
                ),
                ListTile(
                  leading: Icon(Icons.inbox),
                  title: Text("Inbox"),
                ),
                ...List.generate(
                  15,
                  (index) => ListTile(
                    leading: Icon(Icons.inbox),
                    title: Text("Inbox"),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
                Container(
                  child: Text("Footer scroll"),
                ),
              ],
            ),
          ),
          Container(
            child: Text("Footer Fix"),
          ),
        ],
      ),
    );
  }

  Widget _contohEndDrawer() {
    return Drawer(
      child: Column(
        children: [
          AppBar(
            title: Text("Flash Sale"),
            centerTitle: true,
            automaticallyImplyLeading: false,
            actions: [
              Icon(
                Icons.light,
                color: Colors.transparent,
              ),
            ],
          ),
          Expanded(
            child: GridView.count(
              crossAxisCount: 1,
              padding: EdgeInsets.zero,
              childAspectRatio: 16 / 9,
              children: List.generate(10, (index) => Card(child: FlutterLogo())),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contoh Drawer"),
      ),
      drawer: _contohDrawer(),
      // endDrawer: _contohDrawer(),
      // endDrawer: _contohDrawerFix(),
      endDrawer: _contohEndDrawer(),
      onDrawerChanged: (isOpen) {
        print("status drawer $isOpen");
      },
    );
  }
}
