import 'package:flutter/material.dart';
import 'package:helloworld/app_config.dart';
import 'package:helloworld/my_app.dart';

void main() {
  final appConfig = AppConfig("dev", "devsfa.forcapos.xyz", MyApp());

  final banner = Directionality(
    textDirection: TextDirection.ltr,
    child: Banner(
      message: appConfig.tag,
      location: BannerLocation.bottomEnd,
      child: appConfig,
    ),
  );

  runApp(banner);
}
