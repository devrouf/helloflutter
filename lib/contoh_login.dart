/*
 * Create by roufroufrouf on 2021
 * Email: roufroufrouf@gmail.com
 * Last modified 9/17/21, 6:37 PM
 */

import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

class ContohLogin extends StatelessWidget {
  const ContohLogin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("FirstInit")),
      body: Center(
        child: ElevatedButton(
          onPressed: () async {
            await GetStorage.init();
            Widget next = LoginPage();
            GetStorage storage = GetStorage();
            String? token = storage.read("token");
            if (token?.isNotEmpty ?? false) {
              next = HomePage();
            }
            Navigator.push(
              context,
              MaterialPageRoute(builder: (_) => next),
            );
          },
          child: Text("Check"),
        ),
      ),
    );
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Login Page")),
      body: Center(
        child: isLoading
            ? CircularProgressIndicator()
            : ElevatedButton(
                onPressed: () {
                  setState(() {
                    isLoading = true;
                  });
                  GetStorage storage = GetStorage();
                  storage.write("token", "Bearer ${DateTime.now().millisecondsSinceEpoch}");
                  print("set token");
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (_) => HomePage()),
                  );
                },
                child: Text("Login"),
              ),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    GetStorage storage = GetStorage();
    return Scaffold(
      appBar: AppBar(title: Text("Home Page")),
      body: Center(
        child: Column(
          children: [
            Text("Token: ${storage.read("token")}"),
            ElevatedButton(
              onPressed: () async {
                await GetStorage.init();
                GetStorage storage = GetStorage();
                storage.erase();
                Navigator.popUntil(context, (route) => route.isFirst);
              },
              child: Text("Logout"),
            ),
          ],
        ),
      ),
    );
  }
}
