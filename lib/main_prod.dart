import 'package:flutter/material.dart';
import 'package:helloworld/app_config.dart';
import 'package:helloworld/my_app.dart';

void main() {
  final appConfig = AppConfig("prod", "sfa.forcapos.xyz", MyApp());

  runApp(appConfig);
}
