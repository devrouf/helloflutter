void main() async {
  await exampleAwait();
  // await exampleFuture();
}

Future exampleAwait() async {
  print("await start");
  await Future.delayed(Duration(seconds: 3));
  print("await done");
}

void exampleFuture() {
  Future.delayed(Duration(seconds: 3)).then((_) => print("Future done"));
  // Future.delayed(Duration(seconds: 3)).whenComplete(() => print("done"));
  // Future.delayed(Duration(seconds: 3)).then((_) => print("then")).whenComplete(() => print("done"));
  print("Future start");
}