/*
 * Create by roufroufrouf on 2021
 * Email: roufroufrouf@gmail.com
 * Last modified 9/17/21, 12:13 AM
 */

// import 'package:get_storage/get_storage.dart';
//
// main() async {
//   await GetStorage.init();
//   final box = GetStorage();
//   box.read("coba");
//   box.write("coba", "${DateTime.now().millisecondsSinceEpoch}");
// }

import 'package:shared_preferences/shared_preferences.dart';

main() async {
  SharedPreferences.setMockInitialValues({});
  final pref = await SharedPreferences.getInstance();
  pref.getString("coba");
  pref.setString("coba", "${DateTime.now().millisecondsSinceEpoch}");
}
