import 'dart:async';

Future<String> firstAsync() async {
  await Future.delayed(const Duration(seconds: 2));
  return "First!";
}

Future<String> secondAsync() async {
  await Future.delayed(const Duration(seconds: 3));
  return "Second!";
}

Future<String> thirdAsync() async {
  await Future.delayed(const Duration(seconds: 2));
  return "Third!";
}

Future main2() async {
  var f = await firstAsync();
  print(f);
  var s = await secondAsync();
  print(s);
  var t = await thirdAsync();
  print(t);
  print('done');
}

Future main3() async {
  var f = firstAsync();
  var s = secondAsync();
  var t = thirdAsync();
  print(await f);
  print(await s);
  print(await t);
  print('done');
}

Future main4() async {
  try {
    List responses = await Future.wait([firstAsync(), secondAsync(), thirdAsync()]);
    print("cek $responses");
  } catch (e) {
    print(e);
  }
}

Future main5() async {
  var f = firstAsync();
  var s = secondAsync();
  var t = thirdAsync();
  print("cek ${[await f, await s, await t]}");
}

void main() async {
  print("waktu main2= " + DateTime.now().difference((await main2().then((value) => DateTime.now()))).inSeconds.abs().toString());
  print("waktu main3= " + DateTime.now().difference((await main3().then((value) => DateTime.now()))).inSeconds.abs().toString());
  print("waktu main4= " + DateTime.now().difference((await main4().then((value) => DateTime.now()))).inSeconds.abs().toString());
  print("waktu main5= " + DateTime.now().difference((await main5().then((value) => DateTime.now()))).inSeconds.abs().toString());
}
