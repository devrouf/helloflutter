/*
 * Create by roufroufrouf on 2021
 * Email: roufroufrouf@gmail.com
 * Last modified 9/17/21, 12:19 AM
 */

import 'package:flutter_test/flutter_test.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  test('Coba aja', () async {
    // SharedPreferences.setMockInitialValues({"coba": "2345"});
    SharedPreferences.setMockInitialValues({});

    final pref = await SharedPreferences.getInstance();

    pref.setString("coba", "1234");

    expect(pref.getString("coba"), "1234");
    // expect(pref.getString("coba"), "2345");

    // await GetStorage.init();
    // final box = GetStorage();
    //
    // box.write("coba", "2345");
    //
    // expect(box.read("coba"), "2345");
  });
}